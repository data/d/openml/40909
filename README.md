# OpenML dataset: Pen_local

https://www.openml.org/d/40909

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

&quot;This UCI dataset contains the hand- written digits 0&ndash;9 of 45 different writers. All digits are kept, except for the digit 4. From this class, the first 10 instances are kept (similar to [46]). This results in a local anomaly detection task with clusters of different densities and 10 local anomalies, which we refer to as pen-local.&quot; (cite from Goldstein, Markus, and Seiichi Uchida. &quot;A comparative evaluation of unsupervised anomaly detection algorithms for multivariate data.&quot; PloS one 11.4 (2016): e0152173.). This dataset is not the original dataset. The target variable &quot;Target&quot; is relabeled into &quot;Normal&quot; and &quot;Anomaly&quot;.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40909) of an [OpenML dataset](https://www.openml.org/d/40909). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40909/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40909/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40909/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

